package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DamageListGoodsDao {
    void save(@Param("damageListGoodsList") List<DamageListGoods> damageListGoodsList);

    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId );
}
