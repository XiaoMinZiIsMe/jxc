package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

public interface SupplierService {
    Map<String, Object> supplierPage(Integer page, Integer rows, String supplierName);

    ServiceVO<Supplier> supplierSave( String supplierId,Supplier supplier);

    ServiceVO<Supplier> supplierDelete(String ids);
}
