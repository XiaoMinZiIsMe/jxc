package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.atguigu.jxc.service.UserService;
import com.atguigu.jxc.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {


    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    private OverflowListDao overflowListDao;

    @Autowired
    private UserService userService ;

    @Override
    public ServiceVO save(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, HttpServletRequest httpServletRequest) {
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());
        HttpSession session = httpServletRequest.getSession();
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        overflowListDao.save(overflowNumber, overflowList);
        OverflowList overflowList1 = overflowListDao.getOneOverflowListGoods(overflowNumber, overflowList);
        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            overflowListGoods.setOverflowListId(overflowList1.getOverflowListId());
        }
        overflowListGoodsDao.save(overflowListGoodsList);
        return new ServiceVO(100, "操作ok", null);
    }

    @Override
    public Map<String,Object> list(String sTime, String eTime) {
        Map<String ,Object> map = new HashMap<>();
        try {
            Date sDate = DateUtil.StringToDate(sTime, "yyyy-MM-dd");
            Date eDate = DateUtil.StringToDate(eTime, "yyyy-MM-dd");
            List<OverflowList> overflowLists = overflowListDao.list(sDate,eDate);
            for (OverflowList overflowList : overflowLists) {
                User user = userService.getUserById(overflowList.getUserId());
                overflowList.setTrueName(user.getTrueName());
            }
            map.put("rows" , overflowLists) ;
            return map;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.goodsList(overflowListId) ;
        Map<String , Object> map = new HashMap<>();
        map.put("rows" , overflowListGoodsList) ;
        return map ;
    }
}
