package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface OverflowListDao {


    void save(@Param("overflowNumber") String overflowNumber,@Param("overflowList") OverflowList overflowList);

    OverflowList getOneOverflowListGoods(@Param("overflowNumber")String overflowNumber,@Param("overflowList") OverflowList overflowList);

    List<OverflowList> list(@Param("sDate") Date sDate, @Param("eDate") Date eDate);
}
