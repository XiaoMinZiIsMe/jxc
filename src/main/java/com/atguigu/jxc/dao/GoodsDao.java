package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> listInventory(@Param(value = "rows") Integer rows,@Param(value = "setOff") Integer setOff,
                              @Param(value = "codeOrName") String codeOrName, @Param(value = "goodsTypeId") Integer goodsTypeId);

    Integer getTotal(@Param(value = "codeOrName") String codeOrName,
                     @Param(value = "goodsTypeId") Integer goodsTypeId);

    GoodsType getGoodsTypeByPid(@Param(value = "goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsByGoodsFrist(@Param(value = "setOff") Integer setOff, @Param(value = "rows")Integer rows,
                                     @Param(value = "codeOrName")String codeOrName, @Param(value = "goodsTypeId")Integer goodsTypeId);

    List<Goods> getGoodsByGoodsSecond(@Param(value = "setOff") Integer setOff, @Param(value = "rows")Integer rows,
                                      @Param(value = "codeOrName")String codeOrName, @Param(value = "goodsTypeId")Integer goodsTypeId);

    Integer getGoodsByGoodsFristTotal(@Param(value = "codeOrName") String codeOrName,
                                      @Param(value = "goodsTypeId") Integer goodsTypeId);

    Integer getGoodsByGoodsSecondTotal(@Param(value = "codeOrName") String codeOrName,
                                       @Param(value = "goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsByGoodsSon(@Param(value = "setOff") Integer setOff, @Param(value = "rows")Integer rows,
                                   @Param(value = "codeOrName")String codeOrName, @Param(value = "goodsTypeId")Integer goodsTypeId);

    void saveGoods(@Param(value = "goods") Goods goods);

    void updateGoodsById(@Param(value = "goods")Goods goods,@Param(value = "goodsId") Integer goodsId);

    void deleteGoods(@Param(value = "goodsId")Integer goodsId);

    Goods getGoodsByGoodsId(@Param(value = "goodsId")Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param(value = "offSet")Integer offSet, @Param(value = "rows")Integer rows,@Param(value = "nameOrCode") String nameOrCode);

    Integer getNoInventoryQuantityTotal(@Param(value = "nameOrCode")String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param(value = "offSet")Integer offSet,@Param(value = "rows") Integer rows, @Param(value = "nameOrCode")String nameOrCode);

    Integer getHasInventoryQuantityTotal(@Param(value = "nameOrCode")String nameOrCode);

    void saveStock(@Param(value = "goodsId")Integer goodsId, @Param(value = "inventoryQuantity")Integer inventoryQuantity, @Param(value = "purchasingPrice")double purchasingPrice);

    List<Goods> selectListByGoodsTypeId(@Param(value = "goodsTypeId") Integer goodsTypeId);

    void deleteStock(@Param(value = "goodsId") Integer goodsId);

    List<Goods> listAlarm();

    Integer getSaleTotal(@Param("goodsCode") String goodsCode);
}
