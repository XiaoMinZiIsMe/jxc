package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao ;

    @Override
    public Map<String, Object> customerList(Integer page, Integer rows, String customerName) {
        Integer offSet = (page - 1) * rows ;
        List<Customer> customerList = customerDao.customerList(offSet, rows ,customerName) ;
        Integer total = customerDao.getTotal() ;
        Map<String , Object> map = new HashMap<>();
        map.put("total" , total) ;
        map.put("rows" , customerList) ;
        return map ;
    }

    @Override
    public ServiceVO<Customer> customerSave(String customerId, Customer customer) {

        if (StringUtil.isEmpty(customerId)){
            customerDao.customerSave(customer) ;
            return new ServiceVO<Customer>(100, "请求成功" , null) ;
        }else {
            customerDao.updateCustomer(customer ,customerId);
            return new ServiceVO<Customer>( 100 , "请求成功" ,null) ;
        }
    }

    @Override
    public ServiceVO<Customer> customerDelete(String ids) {

        String[] split = ids.split(",");
        List<String> idList = Arrays.asList(split);
        customerDao.customerDelete(idList) ;
        return new ServiceVO<Customer>(100 , "请求成功" ,null) ;
    }
}
