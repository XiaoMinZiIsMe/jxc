package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import com.sun.rowset.internal.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao ;
    @Override
    public Map<String, Object> supplierPage(Integer page, Integer rows, String supplierName) {

        Integer offset = (page - 1) * rows ;
        Integer total = supplierDao.getTotal(offset ,rows ,supplierName) ;
        List<Supplier> supplierList = supplierDao.supplierPage(offset , rows , supplierName) ;
        Map<String , Object> supplierMap = new HashMap<>();
        supplierMap.put("total" , total) ;
        supplierMap.put("rows" , supplierList) ;
        return supplierMap ;
    }

    @Override
    public ServiceVO<Supplier> supplierSave( String supplierId,Supplier supplier ) {
        if (StringUtil.isEmpty(supplierId)){
            supplierDao.supplierSave(supplier) ;
            return new ServiceVO<Supplier>(100, "请求成功" , null) ;
        }else {
            supplierDao.updateSupplier(supplier ,supplierId);
            return new ServiceVO<Supplier>( 100 , "请求成功" ,null) ;
        }

    }

    @Override
    public ServiceVO<Supplier> supplierDelete(String ids) {
        String[] split = ids.split(",");
        List<String> idList = Arrays.asList(split);
        supplierDao.supplierDelete(idList) ;
        return new ServiceVO<Supplier>( 100 , "请求成功" ,null) ;
    }
}
