package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageController {
    @Autowired
    private DamageService damageService ;
    @PostMapping("/save")
    @ResponseBody
    public ServiceVO save(String damageNumber, DamageList damageList, String damageListGoodsStr, HttpServletRequest httpServletRequest){
        return damageService.save(damageNumber,damageList,damageListGoodsStr, httpServletRequest) ;
    }
    @PostMapping("/list")
    @ResponseBody
    public Map<String,Object> list(String  sTime, String  eTime){
        return damageService.list(sTime,eTime) ;
    }
    @PostMapping("/goodsList")
    @ResponseBody
    public Map<String,Object> goodsList(Integer damageListId){
        return damageService.goodsList(damageListId) ;
    }

}
