package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.atguigu.jxc.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        //封装数据到map
        Map<String, Object> map = new HashMap<>();
        //判断页数进行分页查寻
        page = page == 0 ? 1 : page;
        Integer total = goodsDao.getTotal(codeOrName, goodsTypeId);
        Integer setOff = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.listInventory(rows, setOff, codeOrName, goodsTypeId);
        for (Goods goods : goodsList) {
            goods.setSaleTotal(goodsDao.getSaleTotal(goods.getGoodsCode()));
        }
        map.put("total", total);
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> objectMap = new HashMap<>();
        Integer setOff = (page - 1) * rows;
        if (goodsTypeId == null) {
            Integer total = goodsDao.getGoodsByGoodsFristTotal(codeOrName, goodsTypeId);
            List<Goods> goodsList = goodsDao.getGoodsByGoodsFrist(setOff, rows, codeOrName, goodsTypeId);
            objectMap.put("total", total);
            objectMap.put("rows", goodsList);
            return objectMap;
        }
        GoodsType goodsType = goodsDao.getGoodsTypeByPid(goodsTypeId);
        if (goodsType.getPId() < 1) {
            Integer total = goodsDao.getGoodsByGoodsFristTotal(codeOrName, goodsTypeId);
            List<Goods> goodsList = goodsDao.getGoodsByGoodsFrist(setOff, rows, codeOrName, goodsTypeId);
            objectMap.put("total", total);
            objectMap.put("rows", goodsList);
            return objectMap;
        } else if (goodsType.getPId() == 1) {
            Integer total = goodsDao.getGoodsByGoodsSecondTotal(codeOrName, goodsTypeId);
            List<Goods> goodsList = goodsDao.getGoodsByGoodsSecond(setOff, rows, codeOrName, goodsTypeId);
            objectMap.put("total", total);
            objectMap.put("rows", goodsList);
            return objectMap;
        } else {
            Integer total = goodsDao.getTotal(codeOrName, goodsTypeId);
            List<Goods> goodsList = goodsDao.getGoodsByGoodsSon(setOff, rows, codeOrName, goodsTypeId);
            objectMap.put("total", total);
            objectMap.put("rows", goodsList);
            return objectMap;
        }
    }

    @Override
    public ServiceVO saveGoods(Goods goods, Integer goodsId) {
        try {
            if (goodsId == null) {
                goods.setInventoryQuantity(0);
                goods.setState(0);
                goodsDao.saveGoods(goods);
            } else {

                //先查goodsId ---->Goods，在该goods ----->set Goods
                goods.setInventoryQuantity(0);
                goods.setState(0);
                goodsDao.updateGoodsById(goods, goodsId);
            }
            return new ServiceVO(100, "请求成功", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {

        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        Integer state = goods.getState();
        if (state == 0) {
            goodsDao.deleteGoods(goodsId);
            return new ServiceVO(100, "请求成功", null);
        }

        return new ServiceVO(200, "该商品存在不可删除状态（已入库，有进货或者是销售单据）", null);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Integer offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);
        Integer total = goodsDao.getNoInventoryQuantityTotal(nameOrCode);
        Map<String, Object> goodsMap = new HashMap<>();
        goodsMap.put("rows", goodsList);
        goodsMap.put("total", total);
        return goodsMap;

    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Integer offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);
        Integer total = goodsDao.getHasInventoryQuantityTotal(nameOrCode);
        Map<String, Object> goodsMap = new HashMap<>();
        goodsMap.put("rows", goodsList);
        goodsMap.put("total", total);
        return goodsMap;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
       goodsDao.saveStock(goodsId , inventoryQuantity , purchasingPrice) ;
       return new ServiceVO(100 , "修改成功" , null );
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {

        Goods goodsByGoodsId = goodsDao.getGoodsByGoodsId(goodsId);
        if (goodsByGoodsId.getState() != 0){
            return new ServiceVO(200 , "該數據不允許被刪除，存在庫存或者是進貨，銷售訂單" , null ) ;
        }else {
            goodsDao.deleteStock(goodsId) ;
            return  new ServiceVO(100 , "刪除成功" , null ) ;
        }

    }

    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> goodsList = goodsDao.listAlarm();
        Map<String , Object> map = new HashMap<>();
        map.put("rows" , goodsList) ;
        return map;
    }
}
