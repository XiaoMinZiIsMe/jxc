package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface DamageService {
    ServiceVO save(String damageNumber, DamageList damageList, String damageListGoodsStr, HttpServletRequest httpServletRequest);

    Map<String,Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer damageListId);
}
