package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface DamageDao {
    void save(@Param("damageNumber") String damageNumber,@Param("damageList") DamageList damageList);

    DamageList getOneDamage(@Param("damageNumber")String damageNumber,@Param("damageList") DamageList damageList);

    List<DamageList> list(@Param("sDate")Date sDate, @Param("eDate")Date eDate);

}
