package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> customerList(Integer page, Integer rows, String customerName);

    ServiceVO<Customer> customerSave(String customerId, Customer customer);

    ServiceVO<Customer> customerDelete(String ids);
}

