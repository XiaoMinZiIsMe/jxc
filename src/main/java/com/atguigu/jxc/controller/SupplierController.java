package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/supplier")
@RestController
public class SupplierController {

    @Autowired
    private SupplierService supplierService ;
    @PostMapping("/list")
    @ResponseBody
    public Map<String , Object> supplierPage(Integer page, Integer rows, String supplierName){

        return supplierService.supplierPage(page,  rows ,supplierName);
    }
    @ResponseBody
    @PostMapping("/save")
    public ServiceVO<Supplier> supplierSave(@RequestParam(value = "supplierId") String supplierId , Supplier supplier){
        return supplierService.supplierSave( supplierId,supplier) ;
    }

    @ResponseBody
    @PostMapping("/delete")
    public ServiceVO<Supplier> supplierDelete(String ids){
        return supplierService.supplierDelete(ids);
    }
}
