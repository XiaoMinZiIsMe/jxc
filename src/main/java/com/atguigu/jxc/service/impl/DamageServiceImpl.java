package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.UserService;
import com.atguigu.jxc.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageServiceImpl implements DamageService {

    @Autowired
    private DamageDao damageDao ;
    @Autowired
    private UserService userService ;
    @Autowired
    private DamageListGoodsDao damageListGoodsDao ;

    @Override
    @Transactional
    public ServiceVO save(String damageNumber, DamageList damageList, String damageListGoodsStr, HttpServletRequest httpServletRequest) {
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());
        HttpSession session = httpServletRequest.getSession();
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        damageDao.save(damageNumber  ,damageList);

        DamageList damageList1 = damageDao.getOneDamage(damageNumber  ,damageList);
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageList1.getDamageListId());
        }
        damageListGoodsDao.save(damageListGoodsList) ;
        return new ServiceVO(100 , "操作ok" , null );
    }

    @Override
    public Map<String,Object> list(String sTime, String eTime) {
        Map<String ,Object> map = new HashMap<>();
        try {
            Date sDate = DateUtil.StringToDate(sTime, "yyyy-MM-dd");
            Date eDate = DateUtil.StringToDate(eTime, "yyyy-MM-dd");
        List<DamageList> damageLists = damageDao.list(sDate,eDate);
            for (DamageList damageList : damageLists) {
                User user = userService.getUserById(damageList.getUserId());
                damageList.setTrueName(user.getTrueName());
            }
        map.put("rows" , damageLists) ;
        return map;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.goodsList(damageListId) ;
        Map<String , Object> map = new HashMap<>();
        map.put("rows" , damageListGoodsList) ;
        return map ;
    }
}
