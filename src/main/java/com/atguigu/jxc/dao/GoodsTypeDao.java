package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    void saveGoodsType(@Param(value = "goodsTypeName") String goodsTypeName,@Param(value = "pId") Integer pId , @Param(value = "status")Integer status);

    void deleteGoodsType(@Param(value = "goodsTypeId") Integer goodsTypeId);

    GoodsType getGoodsTypeByPid(@Param(value = "pId")Integer pId);

    void updateGoodsTypeStatus(@Param(value = "pId")Integer pId);

    void updateGoodsTypeStatusByDelete(@Param(value = "goodsTypeId") Integer goodsTypeId);

    GoodsType getGoodsTypeById(@Param(value = "goodsTypeId") Integer goodsTypeId);
}
