package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerDao {
    List<Customer> customerList(@Param(value = "offSet") Integer offSet,@Param(value = "rows") Integer rows,@Param(value = "customerName") String customerName);

    Integer getTotal();

    void customerSave(@Param(value = "customer") Customer customer);

    void updateCustomer(@Param(value = "customer") Customer customer, @Param(value = "customerId") String customerId);

    void customerDelete( @Param(value = "idList") List<String> idList);
}
