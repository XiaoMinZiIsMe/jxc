package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService ;

    @PostMapping("/list")
    @ResponseBody
    public Map<String , Object> customerList(Integer page, Integer rows, String  customerName){
        return customerService.customerList(page,rows,customerName) ;
    }
    @ResponseBody
    @PostMapping("/save")
    public ServiceVO<Customer> customerSave( Customer customer , String customerId ){
        return customerService.customerSave( customerId,customer) ;
    }
    @ResponseBody
    @PostMapping("/delete")
    public  ServiceVO<Customer> customerDelete(String  ids){
        return customerService.customerDelete(ids) ;
    }
}
