package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SupplierDao {

    Integer getTotal(Integer offset, Integer rows, String supplierName);

    List<Supplier> supplierPage(@Param(value = "offset") Integer offset, @Param(value = "rows") Integer rows,@Param(value = "supplierName") String supplierName);

    void supplierSave(@Param(value = "supplier") Supplier supplier);

    void updateSupplier(@Param(value = "supplier") Supplier supplier, @Param(value = "supplierId") String supplierId);

    void supplierDelete(@Param(value = "idList") List<String> idList);
}
