package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;

import java.util.ArrayList;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    ServiceVO<GoodsType> saveGoodsType(String goodsTypeName, Integer pId);

    ServiceVO<GoodsType> deleteGoodsType(Integer goodsTypeId);
}
