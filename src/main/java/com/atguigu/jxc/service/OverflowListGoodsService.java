package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface OverflowListGoodsService {
    ServiceVO save(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, HttpServletRequest httpServletRequest);

    Map<String,Object> list(String sTime, String eTime);

    Map<String, Object> goodsList(Integer overflowListId);
}
