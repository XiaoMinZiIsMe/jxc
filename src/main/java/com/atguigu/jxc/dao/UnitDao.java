package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UnitDao {
    List<Unit> getUnit();

}
